// Создайте массив styles с элементами «Джаз» и «Блюз». Добавьте «Рок-н-ролл» в конец.
var styles = ['Джаз', 'Блюз'];

styles = styles.concat('Рок-н-ролл');
document.write(styles);

document.write('<br>');
document.write('<br>');
document.write('<br>');

// Удалите первый элемент массива и покажите его.
var q = styles.shift();
document.write('Удаленный первый элемент массива: ' + q);
document.write('<br>');
document.write('<br>');

// Вставьте «Рэп» и «Регги» в начало массива.

styles.unshift('Рэп', 'Регги');
document.write(styles);

document.write('<br>');
document.write('<br>');
document.write('<br>');

// Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.

styles.splice(styles.length / 2, 1);
styles.splice(styles.length / 2, 0, 'Классика');

document.write(styles);

document.write('<br>');
document.write('<br>');
document.write('<br>');
